"""Access to Minion via REST API.

Reference: https://extranet.atlassian.com/display/Minion

(c) 2015 Atlassian Pty Ltd ALL RIGHTS RESERVED
"""
import slumber
import json
from pylassian import utc_format, ResilientSession


class MinionService(object):
    """Objects for interacting with a Minion service via its REST API.
    """
    @classmethod
    def related(cls, kind, item, api):
        """Converts integer into URI and non-URI string into dict with name.

        :param kind: resource type, e.g. teams, products, releases, etc.
        :param item: identification for resource, e.g. id, name, uri
        :return: URI or {"name": item} or item
        """
        if type(item) is int:
            return "/api/%s/v1/%s/%d" % (api, kind, item)
        elif isinstance(item, basestring) and not item.startswith("/api/%s/v1/%s/" % (api, kind)):
            return {"name": item}
        else:
            return item

    def __init__(self, auth, server):
        """Create wrapper for given user's interactions with specific Minion service.

        :param auth: credentials to use
        :param server: URL of server (default: $MINION_URL or main_url)
        """
        self.server = server
        self.rest = slumber.API(self.server, session=ResilientSession(), auth=auth)
        self.pipeline = self.rest.api.pipeline.v1
        self.process = self.rest.api.process.v1

    def promote_all(self, pending, current, record):
        """Promote all unsuperseded and unpromoted releases from one environment to another.

        :param pending: environment to promote releases from
        :param current: environment to be updated
        """
        # Look up resources by given names.
        pending_id = self.pipeline.environments.get(name=pending)['objects'][0]['id']
        current_id = self.pipeline.environments.get(name=current)['objects'][0]['id']
        # Modify current environment to include releases from pending.
        return self.rest.api.pipeline.v1.environments(current_id).patch({}, promote=pending_id, deploy=record)

    def create_step(self, step, name, started, data):
        self.pipeline.steps.post({"step": step, "name": name, "started": started, "data": data})
        return "Successfully created a start audit log in minion"

    def create_step_duration(self, step, name, started, duration, data):
        self.pipeline.steps.post({
            "step": step,
            "name": name,
            "started": started,
            "duration": duration,
            "data": data,
        })
        return "Successfully recorded an audit log in minion"

    def update_finished(self, name, finished, data):
        self.pipeline.steps.post({"name": name, "finished": finished, "data": data})
        return "Successfully updated an audit log in minion"

    def update_duration(self, name, duration, data):
        self.pipeline.steps.post({"name": name, "duration": duration, "data": data})
        return "Successfully updated an audit log in minion"

    def record_release(self, product, version, repo, team=None, build=None, artifacts=None, started=None):
        """Create/update VersionRelease ready to register VersionPromotes against.

        :param team: team that owns release (ID, URI or dict)
        :param product: product being released (ID, URI or dict)
        :param version: version being released
        :param build: URL of Bamboo build result
        :param artifacts: list of GAVs to save in Released Artefacts field
        :param repo: http address of git repository
        :param started: timestamp when build started
        :return: created/updated VersionRelease
        :rtype: dict
        """
        release = {
            "product": {"name": product, "repo": repo},
            "version": version,
        }
        if artifacts:
            release["artifacts"] = artifacts
        if team:
            release["team"] = self.related("teams", team, 'pipeline')
        if build:
            release["build"] = build
        if started:
            release["started"] = started.strftime(utc_format)

        return self.pipeline.releases.post(release)

    def record_promote(self, release, environment, promoter, issues, qualify, deploy, verify):
        """Create/update VersionPromote associated with VersionRelease.

        :param release: release being promoted (ID, URI or dict)
        :param environment: environment promoted to (ID, URI or dict)
        :param promoter: username of person doing the promote
        :param issues: URL of Bamboo issue list for this deployment
        :param qualify: URL of Bamboo build result for pre-deploy testing
        :param deploy: URL of Bamboo deployment log
        :param verify: URL of post-deploy verification report
        :return: created/updated VersionPromote
        :rtype: dict
        """
        promote = dict(
            release=self.related("releases", release, "pipeline"),
            environment=self.related("environments", environment, "pipeline"),
            issues=issues,
            qualify=qualify,
            deploy=deploy,
            verify=verify,
        )
        if promoter:
            promote['promoter'] = {'username': promoter}
        return self.pipeline.promotes.post(promote)

    def record_test(self, tests_times, data):
        """ Record test audit log
        :param tests_times: open file to load json
        :param data: qualify the entry
        :return: success message
        """
        body = dict(tests=json.load(tests_times), data=json.loads(data))
        self.pipeline.processtiming.test.post(data=body)
        return "Successfully updated an audit log in minion"

    def record_step_timings(self, timing_set):
        return self.process.steps.patch(timing_set)
